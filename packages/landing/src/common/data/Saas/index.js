import Client1 from '../../assets/image/saas/testimonial/ajwad-cader.jpg';
import Client2 from '../../assets/image/saas/testimonial/kumaresan-srinivasan.jpg';

export const Faq = [
  {
    id: 1,
    expend: true,
    title: 'How to contact with Customer Service?',
    description:
      'Our Customer Experience Team is available 7 days a week and we offer 2 ways to get in contact.Email and Chat . We try to reply quickly, so you need not to wait too long for a response!. ',
  },
  {
    id: 2,
    title: 'App installation failed, how to update system information?',
    description:
      'Please read the documentation carefully . We also have some online  video tutorials regarding this issue . If the problem remains, Please Open a ticket in the support forum . ',
  },
  {
    id: 3,
    title: 'Website reponse taking time, how to improve?',
    description:
      'At first, Please check your internet connection . We also have some online  video tutorials regarding this issue . If the problem remains, Please Open a ticket in the support forum .',
  },
  {
    id: 4,
    title: 'New update fixed all bug and issues?',
    description:
      'We are giving the update of this theme continuously . You will receive an email Notification when we push an update. Always try to be updated with us .',
  },
  {
    id: 4,
    title: 'New update fixed all bug and issues?',
    description:
      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
  },
];

export const Features = [
  {
    id: 1,
    icon: 'flaticon-creative',
    title: 'Powerful Features',
    description:
      'Automate time consuming tasks like organising expenses ,tracking your time and following up with clients ',
  },
  {
    id: 2,
    icon: 'flaticon-briefing',
    title: 'Easy Invoicing',
    description:
      'Want to surprice your clients with professional looking invoices ? Then you are some clicks behind .',
  },
  {
    id: 3,
    icon: 'flaticon-flask',
    title: 'Easy To Use',
    description:
      'Very Simple and intuitive. So you have to spend less time in paperwork and impress your customer with looks',
  },
];

export const Footer_Data = [
  {
    title: 'About Us',
    menuItems: [
      {
        url: '#',
        text: 'Support Center',
      },
      {
        url: '#',
        text: 'Customer Support',
      },
      {
        url: '#',
        text: 'About Us',
      },
      {
        url: '#',
        text: 'Copyright',
      },
      {
        url: '#',
        text: 'Popular Campaign',
      },
    ],
  },
  {
    title: 'Our Information',
    menuItems: [
      {
        url: '#',
        text: 'Return Policy',
      },
      {
        url: '#',
        text: 'Privacy Policy',
      },
      {
        url: '#',
        text: 'Terms & Conditions',
      },
      {
        url: '#',
        text: 'Site Map',
      },
      {
        url: '#',
        text: 'Store Hours',
      },
    ],
  },
  {
    title: 'My Account',
    menuItems: [
      {
        url: '#',
        text: 'Press inquiries',
      },
      {
        url: '#',
        text: 'Social media directories',
      },
      {
        url: '#',
        text: 'Images & B-roll',
      },
      {
        url: '#',
        text: 'Permissions',
      },
      {
        url: '#',
        text: 'Speaker requests',
      },
    ],
  },
  {
    title: 'Policy',
    menuItems: [
      {
        url: '#',
        text: 'Application security',
      },
      {
        url: '#',
        text: 'Software principles',
      },
      {
        url: '#',
        text: 'Unwanted software policy',
      },
      {
        url: '#',
        text: 'Responsible supply chain',
      },
    ],
  },
];

export const Service = [
  {
    id: 1,
    icon: 'flaticon-briefing',
    title: 'Malware Analysis (ML)',
    description: 'Machine Learning based Malware Analysis to identify any potential malicious behavior in the mobile application. This will Determine whether the application has come from its original source.',
  },
  {
    id: 2,
    icon: 'flaticon-trophy',
    title: 'Manifest Analysis',
    description: 'Find vulnerability inside one of the components in the AndroidManifest.xml file.',
  },
  {
    id: 3,
    icon: 'flaticon-atom',
    title: 'Static Code Analysis',
    description: 'dentifies potential vulnerabilities, and determines their severity & type of vulnerability found in the lines of code.',
  },
  {
    id: 4,
    icon: 'flaticon-ruler',
    title: 'Sensitive Info Leakage',
    description: 'Identify any potentially sensitive information leakage in the application such as API keys, Passwords etc.',
  },
  {
    id: 5,
    icon: 'flaticon-creative',
    title: 'Binary Analysis',
    description: 'This is analysis of binary code to identify security issues.',
  },
  {
    id: 6,
    icon: 'flaticon-conversation',
    title: 'Android API',
    description: 'You can view android API used in app like java reflection, location, storage and similar stuffs.',
  },
];

export const Timeline = [
  {
    title: 'AI-Based Malware analysis',
    description:
      'Our AI intelligently determines the functionality, origin & potential impact of a given malware sample such as a virus. With new malware created it\'s hard for any single company to keep track of.',
  },
  {
    title: 'Integrated DevSecOps',
    description:
      'Our platform aims to raise security awareness across all levels and implement security measures from the start, within the build pipeline.',
  },
  {
    title: 'Real-Time Reporting',
    description:
      'Awareness of time is very critical for building a secure app. Know the vulnerabilities before each release and continuously monitor the app to know and fix new threats.',
  },
];

export const Testimonial = [
  {
    id: 1,
    name: 'Ajwad Cader',
    designation: 'CEO, HT Works Pvt Ltd',
    comment:
      'We felt comfortable and confident to work with your team from the first meeting. The professionalism and the in-depth understanding of the industry, helped your team listen and understand our requirements better. We were happy that we were kept updated on the progress and the project was delivered as per the agreed timeline and report was comprehensive and helpful. We are also happy with the followup actions and meetings. We would definitely recommend you for Penetration Testing and any Cybersecurity projects.',
    avatar_url: Client1,
    social_icon: '',
  },
  {
    id: 2,
    name: 'Kumaresan Srinivasan',
    designation: 'GM-IT, Talent Pro/Pay Check, UK',
    comment:
      'I would like to express my highly appreciation on your expertise and professionalism. Your quick turn around time really helped us to identify the exact root-cause. I strongly recommend this team for any level of security assessments for your IT environment.',
    avatar_url: Client2,
    social_icon: 'flaticon-twitter-logo-silhouette',
  },
];

export const MENU_ITEMS = [
  {
    label: 'Home',
    path: '#banner_section',
    offset: '70',
  },
  {
    label: 'Service',
    path: '#service_section',
    offset: '70',
  },
  {
    label: 'Feature',
    path: '#feature_section',
    offset: '70',
  },
  {
    label: 'Pricing',
    path: '#pricing_section',
    offset: '70',
  },
  {
    label: 'Testimonial',
    path: '#testimonial_section',
    offset: '70',
  },
  {
    label: 'FAQ',
    path: '#faq_section',
    offset: '70',
  },
];

export const MONTHLY_PRICING_TABLE = [
  {
    freePlan: true,
    name: 'Community',
    description: '',
    price: '$0',
    priceLabel: 'Free trial (7 Days)',
    buttonLabel: 'REGISTER NOW',
    url: 'https://app.mobiheals.com/activatePlan/1',
    listItems: [
      {
        content: '1 Credit(s)',
        includes: 'yes',
      },
      {
        content: 'Source Code Review',
        includes: 'yes',
      },
      {
        content: 'Manifest Analysis',
        includes: 'yes',
      },
      {
        content: 'Binary Analysis',
        includes: 'yes',
      },
      {
        content: 'NIAP Analysis',
        includes: 'yes',
      },
      {
        content: 'Permission Analysis',
        includes: 'yes',
      },
      {
        content: 'File Analysis',
        includes: 'yes',
      },
      {
        content: 'CVSS Score Calculator',
        includes: 'yes',
      },
      {
        content: 'Security Dashboard',
        includes: 'yes',
      },
      {
        content: 'Sensitive Information Leak Analysis',
        includes: 'yes',
      },
      {
        content: 'Android API Visibility',
        includes: 'yes',
      },
      {
        content: 'Malware Analysis (ML Based)',
        includes: 'yes',
      },
      {
        content: 'Browsable Activities',
        includes: 'yes',
      },
      {
        content: 'Role Based Access Controls',
        includes: 'no',
      },
      {
        content: 'Downloadable Reports',
        includes: 'no',
      },
      {
        content: 'Support',
        includes: 'no',
      },
      {
        content: 'API Vulnerability Assessment',
        includes: 'no',
      },
      {
        content: 'Dynamic App Security Testing',
        includes: 'no',
      },
    ],
  },
  {
    name: 'Basic',
    description: '',
    price: '$10',
    priceLabel: 'Per month',
    buttonLabel: 'SUBSCRIBE NOW',
    url: 'https://app.mobiheals.com/activatePlan/2',
    listItems: [
      {
        content: '1 Credit(s)',
        includes: 'yes',
      },
      {
        content: 'Source Code Review',
        includes: 'yes',
      },
      {
        content: 'Manifest Analysis',
        includes: 'yes',
      },
      {
        content: 'Binary Analysis',
        includes: 'yes',
      },
      {
        content: 'NIAP Analysis',
        includes: 'yes',
      },
      {
        content: 'Permission Analysis',
        includes: 'yes',
      },
      {
        content: 'File Analysis',
        includes: 'yes',
      },
      {
        content: 'CVSS Score Calculator',
        includes: 'yes',
      },
      {
        content: 'Security Dashboard',
        includes: 'yes',
      },
      {
        content: 'Sensitive Information Leak Analysis',
        includes: 'no',
      },
      {
        content: 'Android API Visibility',
        includes: 'no',
      },
      {
        content: 'Malware Analysis (ML Based)',
        includes: 'no',
      },
      {
        content: 'Browsable Activities',
        includes: 'no',
      },
      {
        content: 'Role Based Access Controls',
        includes: 'no',
      },
      {
        content: 'Downloadable Reports',
        includes: 'no',
      },
      {
        content: 'Support',
        includes: 'no',
      },
      {
        content: 'API Vulnerability Assessment',
        includes: 'no',
      },
      {
        content: 'Dynamic App Security Testing',
        includes: 'no',
      },
    ],
  },
  {
    name: 'Standard',
    description: '',
    price: '$250',
    priceLabel: 'Per month',
    buttonLabel: 'Subscribe Now',
    url: 'https://app.mobiheals.com/activatePlan/3',
    listItems: [
      {
        content: '1 Credit(s)',
        includes: 'yes',
      },
      {
        content: 'Source Code Review',
        includes: 'yes',
      },
      {
        content: 'Manifest Analysis',
        includes: 'yes',
      },
      {
        content: 'Binary Analysis',
        includes: 'yes',
      },
      {
        content: 'NIAP Analysis',
        includes: 'yes',
      },
      {
        content: 'Permission Analysis',
        includes: 'yes',
      },
      {
        content: 'File Analysis',
        includes: 'yes',
      },
      {
        content: 'CVSS Score Calculator',
        includes: 'yes',
      },
      {
        content: 'Security Dashboard',
        includes: 'yes',
      },
      {
        content: 'Sensitive Information Leak Analysis',
        includes: 'yes',
      },
      {
        content: 'Android API Visibility',
        includes: 'yes',
      },
      {
        content: 'Malware Analysis (ML Based)',
        includes: 'yes',
      },
      {
        content: 'Browsable Activities',
        includes: 'yes',
      },
      {
        content: 'Role Based Access Controls',
        includes: 'yes',
      },
      {
        content: 'Downloadable Reports',
        includes: 'yes',
      },
      {
        content: 'Support',
        includes: 'yes',
      },
      {
        content: 'API Vulnerability Assessment',
        includes: 'no',
      },
      {
        content: 'Dynamic App Security Testing',
        includes: 'no',
      },
    ],
  },
  {
    name: 'Enterprise',
    description: '',
    price: '$$$',
    priceLabel: 'Per month',
    buttonLabel: 'Contact Us',
    url: 'javascript:;',
    listItems: [
      {
        content: 'Unlimited',
        includes: 'yes',
      },
      {
        content: 'Source Code Review',
        includes: 'yes',
      },
      {
        content: 'Manifest Analysis',
        includes: 'yes',
      },
      {
        content: 'Binary Analysis',
        includes: 'yes',
      },
      {
        content: 'NIAP Analysis',
        includes: 'yes',
      },
      {
        content: 'Permission Analysis',
        includes: 'yes',
      },
      {
        content: 'File Analysis',
        includes: 'yes',
      },
      {
        content: 'CVSS Score Calculator',
        includes: 'yes',
      },
      {
        content: 'Security Dashboard',
        includes: 'yes',
      },
      {
        content: 'Sensitive Information Leak Analysis',
        includes: 'yes',
      },
      {
        content: 'Android API Visibility',
        includes: 'yes',
      },
      {
        content: 'Malware Analysis (ML Based)',
        includes: 'yes',
      },
      {
        content: 'Browsable Activities',
        includes: 'yes',
      },
      {
        content: 'Role Based Access Controls',
        includes: 'yes',
      },
      {
        content: 'Downloadable Reports',
        includes: 'yes',
      },
      {
        content: 'Support',
        includes: 'yes',
      },
      {
        content: 'API Vulnerability Assessment',
        includes: 'yes',
      },
      {
        content: 'Dynamic App Security Testing',
        includes: 'yes',
      },
    ],
  },
];

export const YEARLY_PRICING_TABLE = [
  {
    freePlan: true,
    name: 'Basic Account',
    description: 'For a single client or team who need to build website ',
    price: '$0',
    priceLabel: 'Only for first month',
    buttonLabel: 'CREATE FREE ACCOUNT',
    url: '#',
    listItems: [
      {
        content: 'Drag & Drop Builder',
      },
      {
        content: '1,000s of Templates Ready',
      },
      {
        content: 'Blog Tools',
      },
      {
        content: 'eCommerce Store ',
      },
      {
        content: '30+ Webmaster Tools',
      },
    ],
  },
  {
    name: 'Business Account',
    description: 'For Small teams or group who need to build website ',
    price: '$6.00',
    priceLabel: 'Per month & subscription yearly',
    buttonLabel: 'START FREE TRIAL',
    url: '#',
    listItems: [
      {
        content: 'Unlimited secure storage',
      },
      {
        content: '2,000s of Templates Ready',
      },
      {
        content: 'Blog Tools',
      },
      {
        content: '24/7 phone support',
      },
      {
        content: '50+ Webmaster Tools',
      },
    ],
  },
  {
    name: 'Premium Account',
    description: 'For Large teams or group who need to build website ',
    price: '$9.99',
    priceLabel: 'Per month & subscription yearly',
    buttonLabel: 'START FREE TRIAL',
    url: '#',
    listItems: [
      {
        content: 'Drag & Drop Builder',
      },
      {
        content: '3,000s of Templates Ready',
      },
      {
        content: 'Advanced branding',
      },
      {
        content: 'Knowledge base support',
      },
      {
        content: '80+ Webmaster Tools',
      },
    ],
  },
];
