import Heading from 'common/components/Heading';
import Image from 'common/components/Image';
import A from 'common/components/Link';
import Text from 'common/components/Text';
import Container from 'common/components/UI/Container';
import Logo from 'common/components/UIElements/Logo';
import { footerTop } from 'common/data/AppCreative2';
import Link from 'next/link';
import React from 'react';
import { Icon } from 'react-icons-kit';
import { paperPlane } from 'react-icons-kit/fa/paperPlane';
import { ic_phone } from 'react-icons-kit/md/ic_phone';
import { ic_place } from 'react-icons-kit/md/ic_place';
import {
    AboutUs, ContactInfo, FooterWidget, Grid, InfoItem
} from './footer.style';

import LogoImage from 'common/assets/image/saas/logo2.png';

const FooterTop = (props) => {
    const { about, widgets, contactInfo } = footerTop || {};
    return (
        <Container {...props}>
            <div style={{textAlign: "center"}}>
                <Logo href="#" logoSrc={LogoImage} />
                <br></br>
                <br></br>
                <p style={{color: "rgba(15, 33, 55, 0.65)", lineHeight: "24px"}}>Mobile Application SAST (Automated Static Application Security Testing) is an automated, all-in-one mobile application (Android/iOS) White box Security testing, malware analysis and security assessment framework capable of performing static analysis.</p>
                <p style={{color: "rgba(15, 33, 55, 0.65)"}}>The One Tower, Floor #24, Office #12, Sheikh Zayed Road, Dubai, UAE</p>
                <p style={{color: "rgba(15, 33, 55, 0.65)"}}>+971 50 560 2475</p>
            </div>
        </Container>
    );
};

export default FooterTop;
